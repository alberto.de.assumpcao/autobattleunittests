using Microsoft.VisualStudio.TestTools.UnitTesting;
using AutoBattle;
using static AutoBattle.TypesAndHelpers;
using System.Reflection;

namespace AutoBattleTests
{
    [TestClass]
    public class CharacterTests
    {
        [TestMethod]
        public void TestBadSingleCharacterMovement()
        {
            var character = Setup(2, 1, CharacterClass.Warrior, 0, 0);
            object[] parameters = { Direction.right };
            var result = CallCharacterPrivateMethod(character, "CheckDirection", parameters);

            Assert.AreEqual(false, result);
        }
        [TestMethod]
        public void TestGoodSingleCharacterMovement()
        {
            var character = Setup(1, 2, CharacterClass.Warrior, 0, 0);
            object[] parameters = { Direction.right };
            var result = CallCharacterPrivateMethod(character, "CheckDirection", parameters);

            Assert.AreEqual(true, result);
        }

        private Character Setup(int gridLines, int gridColumns, CharacterClass characterClass, int initalX, int initalY)
        {
            var battlefieldMatrix = CreateGrid(gridLines, gridColumns);
            var character = new Character("Test Char", characterClass, 0, battlefieldMatrix);
            character.AlocateCharacterOnGrid(initalX, initalY);

            return character;
        }
        private object CallCharacterPrivateMethod<T>(T objectClass, string method, object[] parameters)
        {
            var methodInfo = typeof(Character).GetMethod(method, BindingFlags.NonPublic | BindingFlags.Instance);
            return methodInfo.Invoke(objectClass, parameters);
        }
    }
}